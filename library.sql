PGDMP     &                    w            library    10.11    10.11 '               0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false                       0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false                       0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false                       1262    16665    library    DATABASE     �   CREATE DATABASE library WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'French_France.1252' LC_CTYPE = 'French_France.1252';
    DROP DATABASE library;
             postgres    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false                       0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    3                        3079    12924    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false                       0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    1            �            1259    16676    book    TABLE     
  CREATE TABLE public.book (
    id integer NOT NULL,
    title character(80) NOT NULL,
    author character(80) NOT NULL,
    editor character(80) NOT NULL,
    summary character varying(300) NOT NULL,
    page_nb integer NOT NULL,
    library_id integer NOT NULL
);
    DROP TABLE public.book;
       public         postgres    false    3            �            1259    16674    book_id_seq    SEQUENCE     �   CREATE SEQUENCE public.book_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 "   DROP SEQUENCE public.book_id_seq;
       public       postgres    false    3    199                       0    0    book_id_seq    SEQUENCE OWNED BY     ;   ALTER SEQUENCE public.book_id_seq OWNED BY public.book.id;
            public       postgres    false    198            �            1259    16668    library    TABLE     �   CREATE TABLE public.library (
    id integer NOT NULL,
    lib_name character(80) NOT NULL,
    addresse character(150) NOT NULL,
    numtel character(14) NOT NULL
);
    DROP TABLE public.library;
       public         postgres    false    3            �            1259    16666    library_id_seq    SEQUENCE     �   CREATE SEQUENCE public.library_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.library_id_seq;
       public       postgres    false    3    197                       0    0    library_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE public.library_id_seq OWNED BY public.library.id;
            public       postgres    false    196            �            1259    16724    rent    TABLE     �   CREATE TABLE public.rent (
    id integer NOT NULL,
    date_pret timestamp without time zone NOT NULL,
    date_fin timestamp without time zone NOT NULL,
    user_id integer,
    book_id integer
);
    DROP TABLE public.rent;
       public         postgres    false    3            �            1259    16722    rent_id_seq    SEQUENCE     �   CREATE SEQUENCE public.rent_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 "   DROP SEQUENCE public.rent_id_seq;
       public       postgres    false    3    203                       0    0    rent_id_seq    SEQUENCE OWNED BY     ;   ALTER SEQUENCE public.rent_id_seq OWNED BY public.rent.id;
            public       postgres    false    202            �            1259    16692    users    TABLE     �   CREATE TABLE public.users (
    id integer NOT NULL,
    prenom character(80) NOT NULL,
    nom character(80) NOT NULL,
    addresse character(150) NOT NULL,
    numtel character(14) NOT NULL,
    mail character(50) NOT NULL
);
    DROP TABLE public.users;
       public         postgres    false    3            �            1259    16690    users_id_seq    SEQUENCE     �   CREATE SEQUENCE public.users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.users_id_seq;
       public       postgres    false    3    201                       0    0    users_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;
            public       postgres    false    200            �
           2604    16679    book id    DEFAULT     b   ALTER TABLE ONLY public.book ALTER COLUMN id SET DEFAULT nextval('public.book_id_seq'::regclass);
 6   ALTER TABLE public.book ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    199    198    199            �
           2604    16671 
   library id    DEFAULT     h   ALTER TABLE ONLY public.library ALTER COLUMN id SET DEFAULT nextval('public.library_id_seq'::regclass);
 9   ALTER TABLE public.library ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    196    197    197            �
           2604    16727    rent id    DEFAULT     b   ALTER TABLE ONLY public.rent ALTER COLUMN id SET DEFAULT nextval('public.rent_id_seq'::regclass);
 6   ALTER TABLE public.rent ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    202    203    203            �
           2604    16695    users id    DEFAULT     d   ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);
 7   ALTER TABLE public.users ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    200    201    201                      0    16676    book 
   TABLE DATA               W   COPY public.book (id, title, author, editor, summary, page_nb, library_id) FROM stdin;
    public       postgres    false    199   m'       
          0    16668    library 
   TABLE DATA               A   COPY public.library (id, lib_name, addresse, numtel) FROM stdin;
    public       postgres    false    197    )                 0    16724    rent 
   TABLE DATA               I   COPY public.rent (id, date_pret, date_fin, user_id, book_id) FROM stdin;
    public       postgres    false    203   �)                 0    16692    users 
   TABLE DATA               H   COPY public.users (id, prenom, nom, addresse, numtel, mail) FROM stdin;
    public       postgres    false    201   �)                  0    0    book_id_seq    SEQUENCE SET     9   SELECT pg_catalog.setval('public.book_id_seq', 3, true);
            public       postgres    false    198                       0    0    library_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('public.library_id_seq', 2, true);
            public       postgres    false    196                       0    0    rent_id_seq    SEQUENCE SET     9   SELECT pg_catalog.setval('public.rent_id_seq', 2, true);
            public       postgres    false    202                        0    0    users_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public.users_id_seq', 2, true);
            public       postgres    false    200            �
           2606    16684    book book_pkey 
   CONSTRAINT     L   ALTER TABLE ONLY public.book
    ADD CONSTRAINT book_pkey PRIMARY KEY (id);
 8   ALTER TABLE ONLY public.book DROP CONSTRAINT book_pkey;
       public         postgres    false    199            �
           2606    16673    library library_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.library
    ADD CONSTRAINT library_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.library DROP CONSTRAINT library_pkey;
       public         postgres    false    197            �
           2606    16729    rent rent_pkey 
   CONSTRAINT     L   ALTER TABLE ONLY public.rent
    ADD CONSTRAINT rent_pkey PRIMARY KEY (id);
 8   ALTER TABLE ONLY public.rent DROP CONSTRAINT rent_pkey;
       public         postgres    false    203            �
           2606    16697    users users_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.users DROP CONSTRAINT users_pkey;
       public         postgres    false    201            �
           2606    16685    book fk_librarybook    FK CONSTRAINT     w   ALTER TABLE ONLY public.book
    ADD CONSTRAINT fk_librarybook FOREIGN KEY (library_id) REFERENCES public.library(id);
 =   ALTER TABLE ONLY public.book DROP CONSTRAINT fk_librarybook;
       public       postgres    false    2694    199    197            �
           2606    16735    rent rent_book_id_fkey    FK CONSTRAINT     t   ALTER TABLE ONLY public.rent
    ADD CONSTRAINT rent_book_id_fkey FOREIGN KEY (book_id) REFERENCES public.book(id);
 @   ALTER TABLE ONLY public.rent DROP CONSTRAINT rent_book_id_fkey;
       public       postgres    false    2696    203    199            �
           2606    16730    rent rent_user_id_fkey    FK CONSTRAINT     u   ALTER TABLE ONLY public.rent
    ADD CONSTRAINT rent_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id);
 @   ALTER TABLE ONLY public.rent DROP CONSTRAINT rent_user_id_fkey;
       public       postgres    false    2698    201    203               �  x����n1�g�SpL��`_����	��x�҅'����ă$�u��F_�OR��u�Ћ&�"?�?���:x�2l�0��'���L1N�������`]���	��L�	c��}(2K�H����3C���v�0Di#�����]x��X�͢��C�h	�m������n:N�+�8I�}����QJ�%��?��G�r��n���0:8q�Hj=*��	�5�ap8����|	7���u��d�W�>\�#�b�ۻk�Ә�0�Z�t�٢ڤ�~Q����Z+���'X�F�S���4�j�����:�w�ǐ:�1܋�3�K��1k��y��%V�6��T�|8R�Z�muj�:~sjA	%�2ps}�~���u!*�p�����cpt$/C�́����Y45�k J���KS_ˢ(� `��F      
      x�3�t�L���/�8���4U!�T�,3''1=U�\�i`�������GӨ��26425��4q��8}2��3�2��N-V��,+R��`a���[�X\11-��y�̤�4012�07�43q�b���� $SV          >   x�3�420��50�54R04�26�22�����̠b���\F�
��誁b&01N#�=... V�         �   x�3�t�/*N�,JU����/-�����G�JS�R�89�(���$ k0 NS3KS#cC07��� �K���m�g@fQb	���
��2*F&�����ļ��Tp$�喦�@��[ G�\��;p��qqq �J�     