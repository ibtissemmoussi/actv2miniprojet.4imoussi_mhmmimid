package com.talanacademy.librairyproject;

import java.util.Arrays;
import java.util.Objects;


public class User implements UserInterface {
	private int id;
	private String prenom;
	private String nom;
	private String address;
	private String numTel;
	private String mail;
	private Rent[] rent;

	@Override
	public void afficherUtilisateur() {
	}

	@Override
	public boolean verifierUtilisateur(User u1) {

		if (this == u1) {
			return true;
		}
		if (!(u1 instanceof User)) {
			return false;
		}
		User other = (User) u1;
		return Objects.equals(address, other.address) && id == other.id && Objects.equals(mail, other.mail)
				&& Objects.equals(nom, other.nom) && Objects.equals(numTel, other.numTel)
				&& Objects.equals(prenom, other.prenom) && Arrays.equals(rent, other.rent);
	}

	public User(int id, String prenom, String nom, String address, String numTel, String mail, Rent[] rent) {
		super();
		this.id = id;
		this.prenom = prenom;
		this.nom = nom;
		this.address = address;
		this.numTel = numTel;
		this.mail = mail;
		this.rent = rent;
	}

	public User() {
		super();
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", prenom=" + prenom + ", nom=" + nom + ", address=" + address + ", numTel=" + numTel
				+ ", mail=" + mail + ", rent=" + Arrays.toString(rent) + "]";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getNumTel() {
		return numTel;
	}

	public void setNumTel(String numTel) {
		this.numTel = numTel;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public Rent[] getRent() {
		return rent;
	}

	public void setRent(Rent[] rent) {
		this.rent = rent;
	}

}
