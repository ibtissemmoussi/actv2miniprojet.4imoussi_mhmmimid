package com.talanacademy.librairyproject;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class LibraryManager {

	public Library getLibraryById(Connection con, int id) {
		Statement st = null;
		Library library = null;
		try {
			st = con.createStatement();
			String getRentByLibrary = "select * from library   where id= " + id;
			ResultSet result = st.executeQuery(getRentByLibrary);
			while (result.next()) {
				library = new Library();
				library.setId(result.getInt("id"));
				library.setName(result.getString("lib_name"));
				library.setNumTel(result.getString("numTel"));
				library.setAddress(result.getString("addresse"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return library;
	}

	// afficher livre de biblio
	public List<Book> afficherLivre(Connection con, Library library) {
		List<Book> ListBook = new ArrayList();
		Statement st = null;
		try {
			st = con.createStatement();
			String afficheLivre = "SELECT * from book where library_id= " + library.getId();
			ResultSet result = st.executeQuery(afficheLivre);
			while (result.next()) {
				Book b = new Book();
				b.setId(result.getInt("id"));
				b.setTitle(result.getString("title"));
				b.setAuthor(result.getString("author"));
				b.setEditor(result.getString("editor"));
				b.setSummary(result.getString("summary"));
				b.setPageNb(result.getInt("page_nb"));
				ListBook.add(b);
			}
			st.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		System.out.println(ListBook);
		return ListBook;
	}

	// tri par author
	public List<Book> triLivreParAuthor(Connection con, Book book) {
		List<Book> ListBookTriee = new ArrayList();
		Statement st = null;

		try {
			st = con.createStatement();
			String triLivreparAuthor = "SELECT  * from book  ORDER BY author";
			ResultSet result = st.executeQuery(triLivreparAuthor);
			while (result.next()) {
				Book b1 = new Book();
				b1.setId(result.getInt("id"));
				b1.setTitle(result.getString("title"));
				b1.setAuthor(result.getString("author"));
				b1.setEditor(result.getString("editor"));
				b1.setSummary(result.getString("summary"));
				b1.setPageNb(result.getInt("page_nb"));
				ListBookTriee.add(b1);
			}
			st.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		System.out.println(ListBookTriee);

		return ListBookTriee;
	}

// recherche par ID
	public List<Book> recherParID(Connection con, int id) {
		List<Book> listBookTrieParID = new ArrayList<Book>();
		Statement st = null;
		try {
			st = con.createStatement();
			String recherchParid = "SELECT * FROM book WHERE id=" + id;
			ResultSet result = st.executeQuery(recherchParid);
			while (result.next()) {
				Book b2 = new Book();
				b2.setId(result.getInt("id"));
				b2.setTitle(result.getString("title"));
				b2.setAuthor(result.getString("author"));
				b2.setEditor(result.getString("editor"));
				b2.setSummary(result.getString("summary"));
				b2.setPageNb(result.getInt("page_nb"));
				listBookTrieParID.add(b2);
			}

		} catch (SQLException e) {
			e.printStackTrace();

		}
		System.out.println(listBookTrieParID);

		return listBookTrieParID;
	}

// recherche par Author 

	public List<Book> rechercheParAuthor(Connection conn, String author) {
		List<Book> listBookParAuthor = new ArrayList<Book>();
		Statement st = null;
		try {
			st = conn.createStatement();
			String rechercheParAuthor = "SELECT * FROM book WHERE author='" + author + "'";
			ResultSet result = st.executeQuery(rechercheParAuthor);
			while (result.next()) {
				Book b3 = new Book();
				b3.setId(result.getInt("id"));
				b3.setTitle(result.getString("title"));
				b3.setAuthor(result.getString("author"));
				b3.setEditor(result.getString("editor"));
				b3.setSummary(result.getString("summary"));
				b3.setPageNb(result.getInt("page_nb"));
				listBookParAuthor.add(b3);

			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		System.out.println(listBookParAuthor);

		return listBookParAuthor;

	}

// Une m�thode permettant d'afficher les livres en cours de pr�t de la biblioth�que pass�e en param�tre;

	public List<Book> getBookInRentbyuser(Connection con, Library library) {
		List<Book> listBook = new ArrayList<>();
		Statement st = null;
		try {
			st = con.createStatement();
			String getRentByLibrary = "select * from rent r,book " + " b where r.book_id=b.id and l.library_id ="
					+ library.getId();
			ResultSet result = st.executeQuery(getRentByLibrary);
			while (result.next()) {
				Book book = new Book();
				book.setId(result.getInt("id"));
				book.setTitle(result.getString("title"));
				book.setAuthor(result.getString("author"));
				book.setEditor(result.getString("editor"));
				book.setPageNb(result.getInt("page_nb"));
				book.setSummary(result.getString("summary"));
				book.setLibrary(getLibraryById(con, result.getInt("library_id")));
				listBook.add(book);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return listBook;
	}

	// M�thode permettant de cr�er un book;

	public void insertBook(Connection con, Book book) {
		Statement st = null;
		try {
			st = con.createStatement();
			String insertBook = "INSERT INTO book (title, author, editor, summary, page_nb, library_id) VALUES ( '"
					+ book.getTitle() + "','" + book.getAuthor() + "', '" + book.getSummary() + "'," + book.getPageNb()
					+ "," + book.getLibrary() + ")";
			System.out.println(st.executeUpdate(insertBook));
		} catch (SQLException e) {

			System.out.println("Failed to close connection.");

		}
	}

// M�thode permettant de cr�er un book;
	public void creatBook(Connection con) {
		Statement st = null;
		try {
			st = con.createStatement();
			String createBook = "CREATE TABLE   book( id SERIAL   PRIMARY KEY NOT NULL ,"
					+ "title varchar(255) NOT NULL," + "author varchar(255) NOT NULL," + "editor varchar(255) NOT NULL,"
					+ "pageNb int NOT NULL," + "summary varchar(255) NOT NULL,"
					+ "library_id int NOT NULL  REFERENCES library(id)";
			System.out.println(st.execute(createBook));
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	// M�thode permettant de cr�er un utilisateur;
	public void creatUser(Connection con) {
		Statement st = null;
		try {
			st = con.createStatement();
			String createUser = "CREATE TABLE   users( id SERIAL   PRIMARY KEY NOT NULL ,"
					+ "prenom varchar(255) NOT NULL," + "nom varchar(255) NOT NULL," + "addresse varchar(255) NOT NULL,"
					+ "numTel varchar(20) NOT NULL," + "mail varchar(255) NOT NULL";
			System.out.println(st.execute(createUser));
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	// M�thode permettant de cr�er un Rent;
	public void creatRent(Connection con) {
		Statement st = null;
		try {
			st = con.createStatement();
			String createRent = "CREATE TABLE   rent( id SERIAL   PRIMARY KEY NOT NULL ,"
					+ "date_pret DATETIME NOT NULL," + "date_fin DATETIME NOT NULL," + "user_id int NOT NULL,"
					+ "pageNb int NOT NULL," + "user_id  int NOT NULL  REFERENCES users(id)"
					+ "book_id int NOT NULL REFERENCES book(id)";
			System.out.println(st.execute(createRent));
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	// M�thode permettant de cr�er un Rent;

	public void insertRent(Connection con, Rent rent) {
		Statement st = null;
		try {
			st = con.createStatement();
			String insertRent = "INSERT INTO Rent (date_pret, date_fin, user_id, book_id) VALUES ( '"
					+ rent.getDatePret() + "','" + rent.getDateFin() + "', '" + rent.getUser().getId() + "',"
					+ rent.getBook().getId() + ")";
			System.out.println(st.executeUpdate(insertRent));
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	// M�thode permettant de trier tous les pr�ts par date de fin d�croissante d'une
	// library pass�e en param�tre;
	public List<Rent> trieRent(Connection con, Library library) {
		Statement st = null;
		List<Rent> listRent = new ArrayList<>();
		try {
			st = con.createStatement();
			String getRentByLibrary = "select * from rent r , book  b where r.book_id=b.id and b.library_id ="
					+ library .getId()+ " order by date_fin desc";
			ResultSet result = st.executeQuery(getRentByLibrary);
			while (result.next()) {
				Rent r = new Rent();
				r.setDateFin(result.getDate("date_pret"));
				r.setDatePret(result.getDate("date_fin"));
				r.setId(result.getInt("id")); 
				r.setUser(getUserById(con, result.getInt("user_id")));
				r.setBook(getBookById(con, result.getInt("book_id")));
				listRent.add(r);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return listRent;
	}

//M�thode permettant de rechercher un utilisateur par son ID;
	public User getUserById(Connection con, int id) {
		Statement st = null;
		User user = null;
		try {
			st = con.createStatement();
			String getRentByLibrary = "select * from users where id= " + id;
			ResultSet result = st.executeQuery(getRentByLibrary);
			while (result.next()) {
				user = new User();
				user.setId(result.getInt("id"));
				user.setNom(result.getString("nom"));
				user.setMail(result.getString("mail"));
				user.setNumTel(result.getString("numTel"));
				user.setAddress(result.getString("addresse"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return user;
	}

//M�thode permettant d'afficher les livres en cours de pr�t d'un utilisateur pass� en param�tre;
	public List<Book> getBookInRentbyuser(Connection con, User user) {
		Statement st = null;
		List<Book> listBook = new ArrayList<>();
		try {
			st = con.createStatement();
			String getRentByLibrary = "select * from rent r,book " + " b where r.book_id=b.id and r.user_id ="
					+ user.getId();
			ResultSet result = st.executeQuery(getRentByLibrary);
			while (result.next()) {
				Book book = new Book();
				book.setId(result.getInt("id"));
				book.setTitle(result.getString("title"));
				book.setAuthor(result.getString("author"));
				book.setEditor(result.getString("editor"));
				book.setPageNb(result.getInt("page_nb"));
				book.setSummary(result.getString("summary"));
				book.setLibrary(getLibraryById(con, result.getInt("library_id")));
				listBook.add(book);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return listBook;
	}

	public Book getBookById(Connection con, int id) {
		Statement st = null;
		Book book = null;
		try {
			st = con.createStatement();
			String getRentByLibrary = "select * from book   where id= " + id;
			ResultSet result = st.executeQuery(getRentByLibrary);
			while (result.next()) {
				book = new Book();
				book.setId(result.getInt("id"));
				book.setTitle(result.getString("title"));
				book.setAuthor(result.getString("author"));
				book.setEditor(result.getString("editor"));
				book.setPageNb(result.getInt("page_nb"));
				book.setSummary(result.getString("summary"));
				book.setLibrary(getLibraryById(con, result.getInt("library_id")));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return book;
	}

// ==================================== Class MAIN ===================================================================	

	public static void main(String[] args) {
		ConnectionManager connectionManager = new ConnectionManager();
		Connection conn = connectionManager.getConnection();
		LibraryManager libraryManager = new LibraryManager();
		System.out.println("****** Affichage de la bibliotheque qui a l'id 1");
		Library library = libraryManager.getLibraryById(conn, 1);
		System.out.println("****** trier tous les pr�ts par date de fin d�croissante d'une  library");
		System.out.println(libraryManager.trieRent(conn, library));
		System.out.println("****** Affichage de l'utisateur qui a l'id 1");
		System.out.println(libraryManager.getUserById(conn, 1));
		
		Library l = libraryManager.getLibraryById(conn, 2);

		System.out.println("**** Affichage Livre ****");
		libraryManager.afficherLivre(conn, l);

		System.out.println("**** Affichage Livre Trier par Author ****");

		Book book = libraryManager.getBookById(conn, 1);
		libraryManager.triLivreParAuthor(conn, book);

		System.out.println("**** Recherche livre par ID ****");
		libraryManager.recherParID(conn, 1);

		System.out.println("**** Recherche livre par Author ****");
		libraryManager.rechercheParAuthor(conn, "Kathy Sierra");
		try {
			conn.close();
		} catch (SQLException e) {
			System.out.println("Failed to close connection.");

		}
	}
}
