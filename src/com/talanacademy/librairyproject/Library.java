package com.talanacademy.librairyproject;

import java.util.Arrays;

public class Library {

	private int id;
	private String name;
	private String address;
	private String numTel;
	private Book[] book;
	private Rent[] rent;

	public Library(int id, String name, String address, String numTel, Book[] book, Rent[] rent) {
		super();
		this.id = id;
		this.name = name;
		this.address = address;
		this.numTel = numTel;
		this.book = book;
		this.rent = rent;
	}

	public Library() {
		super();
	}

	@Override
	public String toString() {
		return "Library [id=" + id + ", name=" + name + ", address=" + address + ", numTel=" + numTel + ", book="
				+ Arrays.toString(book) + ", rent=" + Arrays.toString(rent) + "]";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getNumTel() {
		return numTel;
	}

	public void setNumTel(String numTel) {
		this.numTel = numTel;
	}

	public Book[] getBook() {
		return book;
	}

	public void setBook(Book[] book) {
		this.book = book;
	}

	public Rent[] getRent() {
		return rent;
	}

	public void setRent(Rent[] rent) {
		this.rent = rent;
	}

}
