package com.talanacademy.librairyproject;

import java.sql.Date;
import java.util.Objects;

public class Rent implements RentInterface {

	private int id;
	private Date datePret;
	private Date DateFin;
	private User user;
	private Book book;

	@Override
	public void afficherPret() {

	}

	@Override
	public boolean verifierPret(Rent r1) {
		if (this == r1) {
			return true;
		}
		if (!(r1 instanceof Rent)) {
			return false;
		}
		Rent other = (Rent) r1;
		return Objects.equals(DateFin, other.DateFin) && Objects.equals(book, other.book)
				&& Objects.equals(datePret, other.datePret) && id == other.id && Objects.equals(user, other.user);
	}

	public Rent(int id, Date datePret, Date dateFin, User user, Book book) {
		super();
		this.id = id;
		this.datePret = datePret;
		DateFin = dateFin;
		this.user = user;
		this.book = book;
	}

	public Rent() {
		super();
	}

	@Override
	public String toString() {
		return "Rent [id=" + id + ", datePret=" + datePret + ", DateFin=" + DateFin + ", user=" + user + ", book="
				+ book + "]\n";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDatePret() {
		return datePret;
	}

	public void setDatePret(Date datePret) {
		this.datePret = datePret;
	}

	public Date getDateFin() {
		return DateFin;
	}

	public void setDateFin(Date dateFin) {
		DateFin = dateFin;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

}
