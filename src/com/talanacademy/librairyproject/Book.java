package com.talanacademy.librairyproject;

import java.util.Objects;

public class Book implements BookInterface {
	private int id;
	private String title;
	private String author;
	private String editor;
	private int pageNb;
	private String summary;
	private Library library;

	// méthodes implémentées interface BookInterface
	@Override
	public void afficherLivre() {

	}

	@Override
	public boolean verifierBook(Book b1) {
		if (this == b1) {
			return true;
		}
		if (!(b1 instanceof Book)) {
			return false;
		}
		Book other = (Book) b1;
		return Objects.equals(author, other.author) && Objects.equals(editor, other.editor) && id == other.id
				&& Objects.equals(library, other.library) && pageNb == other.pageNb
				&& Objects.equals(summary, other.summary) && Objects.equals(title, other.title);
	}

	public Book(int id, String title, String author, String editor, int pageNb, String summary, Library library) {
		super();
		this.id = id;
		this.title = title;
		this.author = author;
		this.editor = editor;
		this.pageNb = pageNb;
		this.summary = summary;
		this.library = library;
	}

	public Book() {
		super();
	}

	@Override
	public String toString() {
		return "Book [id=" + id + ", title=" + title + ", author=" + author + ", editor=" + editor + ", pageNb="
				+ pageNb + ", summary=" + summary + ", library=" + library + "]\n";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getEditor() {
		return editor;
	}

	public void setEditor(String editor) {
		this.editor = editor;
	}

	public int getPageNb() {
		return pageNb;
	}

	public void setPageNb(int pageNb) {
		this.pageNb = pageNb;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public Library getLibrary() {
		return library;
	}

	public void setLibrary(Library library) {
		this.library = library;
	}

}
